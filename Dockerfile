#setup instructions for our own image

#base image
FROM node

#set working directory - all commands will be executed inside this folder
WORKDIR /app

#copy package.json file from local machine to image, that has list of dependencies
COPY package.json /app

#run npm install in order to install all dependencies. 
#Doing this before copying all files, so that if we change some file,
#we don't have to install all dependencies again
RUN npm install

#which files should go into image
#First dot - path on local machine
#Second dot - path in image
COPY . /app

#When container is started, we want to expose port 80
EXPOSE 80

#start the application - difference between RUN and CMD is that 
#RUN is started when image is created, and 
#CMD is started when container is created. Here we pass an array
CMD ["node", "server.js"]